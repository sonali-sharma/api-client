# README #

This is a CLI app that calls the API [http://c19api.azurewebsites.net/api/stats/Australia](http://c19api.azurewebsites.net/api/stats/Australia) to get COVID-19 cases for a country.

This app only gets cases for `Australia`. As a future enhancement the CLI app can accept country as CLI argument and display results for a specific country. `CovidApiClient.ts` has the code that can take care of returning cases by country.

## To run this project

### Install packages
`npm install`

### Start
`npm start`

## Unit test
`npm test`
