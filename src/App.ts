import CovidHttpClient from './clients/CovidApiClient';
import CovidResponseItem from './models/CovidResponseItem'

CovidHttpClient.getCovidResultsByCountry("Australia")
.then((response: CovidResponseItem) => {
    console.log(response);
})
.catch((e: any) => { console.error('ERR', e); });
