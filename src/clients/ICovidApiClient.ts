import CovidResponseItem from '../models/CovidResponseItem';

export default interface ICovidApiClient {
    getCovidResultsByCountry(country: string): Promise<CovidResponseItem>
  }