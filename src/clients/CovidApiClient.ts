import axios from 'axios';
import CovidResponseItem from '../models/CovidResponseItem';
import ICovidApiClient from './ICovidApiClient';

class CovidApiClient implements ICovidApiClient {
    private apiUrl: string = "http://c19api.azurewebsites.net/api/stats/";

    async getCovidResultsByCountry(country: string): Promise<CovidResponseItem>{
        if (country)
            this.apiUrl += country;
        else
            return Promise.reject("Country must be set to a value.");

        try
        {
            const axiosResponse = await axios.get<CovidResponseItem>(this.apiUrl);

            if(axiosResponse?.data)
                return Promise.resolve(axiosResponse.data);
            else {
                return Promise.reject("No data");
            }
        } catch(error){
            if (error?.response?.status === 500) {
                return Promise.reject("API returned an internal error");
            } else if (error?.response?.status) {
                return Promise.reject("API returned a non success response of " + error.response.status);
            }
            else {
                return Promise.reject(error);
            }
        }
    }
}

const CovidHttpClient: ICovidApiClient = new CovidApiClient();
export default CovidHttpClient;