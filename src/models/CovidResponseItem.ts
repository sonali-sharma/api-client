export default interface ICovidResponseItem {
    confirmed: number;
    recovered: number;
    deaths: number;
    population: number;
    country: string;
    life_expectancy: string;
}