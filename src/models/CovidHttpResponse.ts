import CovidResponseItem from './CovidResponseItem';

export default interface ICovidHttpResponse {
    data: CovidResponseItem;
}