import CovidHttpClient from '../src/clients/CovidApiClient'
import axios from 'axios';

jest.mock('axios');
const mockedAxios = axios as jest.Mocked<typeof axios>;

const apiUrl = "http://c19api.azurewebsites.net/api/stats/"

const mockedCountryData = {
        confirmed: 28918,
        recovered: 22874,
        deaths: 909,
        country: "Australia",
        life_expectancy: "79.8"
};

describe("When getCovidResultsByCountry method is called", () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    describe("and country has a valid value", () => {
        it('returns response for the country', async () => {
            mockedAxios.get.mockResolvedValue({ status: 200, data: mockedCountryData });

            const validCountry = "Australia";

            const response = await CovidHttpClient.getCovidResultsByCountry(validCountry);

            expect(response.country).toBe(validCountry);
            expect(response.confirmed).toBe(mockedCountryData.confirmed);
            expect(response.recovered).toBe(mockedCountryData.recovered);
            expect(response.life_expectancy).toBe(mockedCountryData.life_expectancy);

            // Assert whether GET API was called with correct arguments
            const mockCall = mockedAxios.get.mock.calls[0];
            expect(mockCall[0]).toBe(apiUrl + validCountry);
        });
    });

    describe("and country has an invalid value", () => {
        it('returns \"No data\" error', async () => {
            mockedAxios.get.mockResolvedValue({ status: 200, data: null });

            const invalidCountry = "Straya";

            let response: any;
            try {
                response = await CovidHttpClient.getCovidResultsByCountry(invalidCountry);
            } catch (error) {
                expect(error).toBe("No data");
            }

            expect(response).toBeUndefined();
        });
    });

    describe("and country has no value", () => {
        it('returns missing value error', async () => {
            let response: any;
            try {
                response = await CovidHttpClient.getCovidResultsByCountry('');
            } catch (error) {
                expect(error).toBe("Country must be set to a value.");
            }

            expect(response).toBeUndefined();
        });
    });

    describe("and API returned HTTP 500", () => {
        it('returns error message', async () => {
            mockedAxios.get.mockRejectedValue({ response: { status: 500 }});

            const validCountry = "Australia";

            let response: any;
            try {
                response = await CovidHttpClient.getCovidResultsByCountry(validCountry);
            } catch (error) {
                expect(error).toBe("API returned an internal error");
            }

            expect(response).toBeUndefined();
        });
    });

    describe("and API returned non success HTTP status other than 500", () => {
        it('returns error message', async () => {
            mockedAxios.get.mockRejectedValue({ response: { status: 400 }});

            const validCountry = "Australia";

            let response: any;
            try {
                response = await CovidHttpClient.getCovidResultsByCountry(validCountry);
            } catch (error) {
                expect(error).toBe("API returned a non success response of 400");
            }

            expect(response).toBeUndefined();
        });
    });

    describe("and API returned unknown error", () => {
        it('returns error message', async () => {
            mockedAxios.get.mockRejectedValue("AN ERROR");

            const validCountry = "Australia";

            let response: any;
            try {
                response = await CovidHttpClient.getCovidResultsByCountry(validCountry);
            } catch (error) {
                expect(error).toBe("AN ERROR");
            }

            expect(response).toBeUndefined();
        });
    });
});